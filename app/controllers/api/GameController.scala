package controllers.api

import dto._
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc._

import scala.collection.mutable


class GameController extends Controller {

  val games: scala.collection.mutable.Map[String, Board] = mutable.Map[String, Board]()

  def start = Action(BodyParsers.parse.json) { request =>
    request.body.validate[Game] match {
      case s: JsSuccess[Game] =>
        games.put(s.get.id, s.get.board)

        println(games)

        Ok(Json.toJson(ResponseStatus("ok")))

      case e: JsError => BadRequest
    }
  }

  def makeMove(gameId: String) = Action { request =>
    request.getQueryString("color") match {
      case Some(c) =>
        val color = c.toInt

        println(s"~~~~~~~~~~ $color")
        val board: Board = games.get(gameId).get
        val max: (Int, Int) = board.sizes
          .filter(t => board.empty(t._1))
          .filter(t => board.canMove(t._1, color))
          .maxBy(t => t._2)
        max

        println(s"~~~~~~~~~~ $max")

        val move = ClientMove(figure = max._1)

        println("###############")
        println("MY MOVE:")
        board.move(max._1, color)



        println(s"~~~~~~~~~~ $move")


        println("makeMove endpoint hit")
        println("gameId=" + gameId)
        println("color=" + color)

        Ok(Json.toJson(move))

      case None => BadRequest
    }
  }

  def handleMove(gameId: String) = Action(BodyParsers.parse.json) { request =>
    request.body.validate[ServerMove] match {
      case s: JsSuccess[ServerMove] =>
        val serverMove = s.get
        println(s"~~~~~~~~~~ $serverMove")

        val board: Board = games.get(gameId).get
        board.move(serverMove.figure, serverMove.color)
        println("handleMove endpoint hit")
        println("gameId=" + gameId)
        println(serverMove)

        Ok(Json.toJson(ResponseStatus("ok")))

      case e: JsError => BadRequest
    }
  }

  def finish(gameId: String) = Action { request =>
    System.out.println("finishGame endpoint hit")
    println("gameId=" + gameId)

    Ok(Json.toJson(ResponseStatus("ok")))
  }
}
