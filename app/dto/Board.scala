package dto

import java.util

import play.api.libs.json.Json

import scala.collection.mutable

object Board {

  implicit val format = Json.format[Board]

}

case class Board(width: Int, height: Int, figures_count: Int, cells: Array[Array[Int]]) {
  override def toString: String =
    "Board{" + "width=" + width + ", height=" + height +
    ", figures_count=" + figures_count +
    ", cells=" + util.Arrays.deepToString(cells.asInstanceOf[Array[AnyRef]])  + "}"

  implicit object order extends Ordering[Int] { def compare(o1: Int, o2: Int) = o2 - o1 }

  val sizes: Map[Int, Int] = (1 to figures_count).map(i => i -> cells.flatten.count(_ == i)).toMap[Int, Int]

  val colors = mutable.Map[Int, Int]()

  def move(figure: Int, color: Int): Boolean = {
    if (colors.isDefinedAt(figure)) {
      false
    } else {
      colors.put(figure, color)
      for (i <- 1 until cells.length) {
        if (i % 2  == 1) {
          print(" ")
        }
        val seq: Array[String] = cells(i).map((x: Int) => colors.get(x).map(_.toString).getOrElse("-"))
        println(seq.mkString(" "))
      }
      true
    }
  }

  def empty(figure: Int): Boolean = !colors.isDefinedAt(figure)

  def canMove(figure: Int, colour: Int): Boolean = {
    if (colors.isDefinedAt(figure)) {
      return false
    }
    for (i <- 1 until cells.length; j <- 1 until cells(0).length; if cells(i)(j) == figure ) {
      if (!canColor(i, j, colour)) return false
    }
    true
  }

  def canColor(i: Int, j: Int, color: Int) = {
    if (i % 2 == 0) {
      (ok(i - 1, j, color)
        && ok(i - 1, j - 1, color)
        && ok(i, j - 1, color)
        && ok(i, j + 1, color)
        && ok(i + 1, j, color)
        && ok(i + 1, j - 1, color))
    } else {
      (ok(i - 1, j, color)
        && ok(i - 1, j + 1, color)
        && ok(i, j - 1, color)
        && ok(i, j + 1, color)
        && ok(i + 1, j, color)
        && ok(i + 1, j + 1, color))

    }
  }

  def ok(i: Int, j: Int, color: Int): Boolean = {
    if (i < 0 || j < 0 || i >= cells.length || j >= cells(0).length)
      return true
    val maybeInt: Option[Int] = colors.get(cells(i)(j))
    if (maybeInt.isEmpty || maybeInt.get != color) {
      true
    } else {
      false
    }
  }
}
